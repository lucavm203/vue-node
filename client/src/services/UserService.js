const axios = require('axios');

export async function getAllToDos() {

    const response = await axios.get('/api/todos');
    return response.data;
}

export async function createToDO(data) {
    const response = await axios.post(`/api/todoadd`, {todoadd: data});
    return response.data;
}

export async function removeToDo(data){
    const response = await axios.post(`/api/todoremove`, {todoremove: data});
    return response.data;
}
export async function doneRandA(data){
    const response = await axios.post(`/api/tododra`, {tododra: data});
    return response.data;
}