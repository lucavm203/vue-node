// server/server.js
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const randomId = require('random-id');
const { Array } = require('core-js');
const app = express();
const port = 8000;

// place holder for the data
// const users = [
//   {
//     id: "1",
//     firstName: "first1",
//     lastName: "last1",
//     email: "abc@gmail.com"
//   },
//   {
//     id: "2",
//     firstName: "first2",
//     lastName: "last2",
//     email: "abc@gmail.com"
//   },
//   {
//     id: "3",
//     firstName: "first3",
//     lastName: "last3",
//     email: "abc@gmail.com"
//   }
// ];

app.use(bodyParser.json());
app.use(express.static(process.cwd() + '/my-app/dist'));

app.get('/api/todos', (req, res) => {
  console.log('api/todos called!!!!!!!')
  fs.readFile('database.json', (err, data) => {
    if (err) throw err;
    let todor = JSON.parse(data);
    console.log(todor);
    res.json(todor);
  });
  
});

app.post('/api/todoadd', (req, res) => {
  fs.readFile('database.json', (err, data) => {
    if (err) throw err;
    let todor = JSON.parse(data);
    res.json(todor);
    console.log(todor);
    const todoadd = req.body.todoadd;
    todoadd.id = randomId(10);
    console.log('Adding user:::::', todoadd);
    todor.push(todoadd);
    console.log(todor);
    todor.forEach((todors) => {
      console.log(todors.id);
    });
    fs.writeFile('database.json', JSON.stringify(todor), function (err) {
      if (err) throw err; 
      console.log('Updated!');
      
      res.json("user addedd");
    });
  });
 
  
});
app.post('/api/tododra', (req, res) => {
  fs.readFile('database.json', (err, data) => {
    if (err) throw err;
    let todor = JSON.parse(data);
    res.json(todor);
    console.log(todor);
    const tododra = req.body.tododra;
    let arraylength = todor.length;
    let todor2 = [];
    console.log(tododra.id);
    for( let i = 0; i < arraylength; i++){
      if(todor[i].id == tododra.id){
        console.log("remove");
        todor2.push(tododra);
      }else{
        todor2.push(todor[i]);
      }
    }
    console.log(todor2);
    fs.writeFile('database.json', JSON.stringify(todor2), function (err) {
      if (err) throw err;
      console.log('Updated!');
      
      res.json("done removed or added to database");
    });
  });
 
  
});
app.post('/api/todoremove', (req, res) => {
  fs.readFile('database.json', (err, data) => {
    if (err) throw err;
    let todor = JSON.parse(data);
    res.json(todor);
    console.log(todor);
    const todoremove = req.body.todoremove;
    let arraylength = todor.length;
    let todor2 = [];
    console.log(todoremove.id);
    for( let i = 0; i < arraylength; i++){
      if(todor[i].id == todoremove.id){
        console.log("remove");
      }else{
        todor2.push(todor[i]);
      }
    }
    console.log(todor2);
    fs.writeFile('database.json', JSON.stringify(todor2), function (err) {
      if (err) throw err;
      console.log('Updated!');
      
      res.json("todo removed form database");
    });
  });
 
  
});

app.get('/', (req,res) => {
  res.sendFile(process.cwd() + '/my-app/dist/index.html');
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});